/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 50530
 Source Host           : localhost:3306
 Source Schema         : dorm

 Target Server Type    : MySQL
 Target Server Version : 50530
 File Encoding         : 65001

 Date: 12/06/2019 21:50:58
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for dorm
-- ----------------------------
DROP TABLE IF EXISTS `dorm`;
CREATE TABLE `dorm`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '宿舍号',
  `dormname` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '宿舍楼名',
  `dormno` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '宿舍楼号',
  `roomno` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '房间号',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `dormno`(`dormno`) USING BTREE,
  INDEX `dormname`(`dormname`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '宿舍楼信息表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of dorm
-- ----------------------------
INSERT INTO `dorm` VALUES ('A1-101', 'A', '1', '101');
INSERT INTO `dorm` VALUES ('A1-102', 'A', '1', '102');
INSERT INTO `dorm` VALUES ('A1-103', 'A', '1', '103');
INSERT INTO `dorm` VALUES ('A1-104', 'A', '1', '104');
INSERT INTO `dorm` VALUES ('A1-105', 'A', '1', '105');
INSERT INTO `dorm` VALUES ('A2-101', 'A', '2', '101');
INSERT INTO `dorm` VALUES ('A2-102', 'A', '2', '102');
INSERT INTO `dorm` VALUES ('A2-103', 'A', '2', '103');
INSERT INTO `dorm` VALUES ('A2-104', 'A', '2', '104');
INSERT INTO `dorm` VALUES ('A2-105', 'A', '2', '105');
INSERT INTO `dorm` VALUES ('B1-101', 'B', '1', '101');
INSERT INTO `dorm` VALUES ('B1-102', 'B', '1', '102');
INSERT INTO `dorm` VALUES ('B1-103', 'B', '1', '103');
INSERT INTO `dorm` VALUES ('B1-104', 'B', '1', '104');
INSERT INTO `dorm` VALUES ('B1-105', 'B', '1', '105');
INSERT INTO `dorm` VALUES ('B2-101', 'B', '2', '101');
INSERT INTO `dorm` VALUES ('B2-102', 'B', '2', '102');
INSERT INTO `dorm` VALUES ('B2-103', 'B', '2', '103');
INSERT INTO `dorm` VALUES ('B2-104', 'B', '2', '104');
INSERT INTO `dorm` VALUES ('B2-105', 'B', '2', '105');

-- ----------------------------
-- Table structure for external
-- ----------------------------
DROP TABLE IF EXISTS `external`;
CREATE TABLE `external`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `name` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `sex` int(2) NOT NULL COMMENT '0男1女',
  `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `stuname` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '被访学生姓名',
  `visittime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '来访时间',
  `endtime` datetime NULL DEFAULT '0000-00-00 00:00:00' COMMENT '结束时间',
  `dormname` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '外来人员访问表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of external
-- ----------------------------
INSERT INTO `external` VALUES ('4028aba26afeb7fb016afed755ee0011', '张三', 0, '1110', '石佛寺', '2019-05-28 22:28:06', '2019-06-06 23:44:11', 'A1');
INSERT INTO `external` VALUES ('4028aba26afeb7fb016afee134130012', '张三', 0, '110', '石佛寺', '2019-05-28 22:38:31', '2019-06-06 23:44:13', 'A1');

-- ----------------------------
-- Table structure for inregister
-- ----------------------------
DROP TABLE IF EXISTS `inregister`;
CREATE TABLE `inregister`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号',
  `stuid` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '学生id',
  `staffid` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '管理员id',
  `rebatchid` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '登记时间批次',
  `arrivetime` datetime NULL DEFAULT '0000-00-00 00:00:00' COMMENT '到校时间',
  `status` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '状态，0 创建 1 填写完毕 2 历史',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `stuid`(`stuid`) USING BTREE,
  CONSTRAINT `inregister_ibfk_1` FOREIGN KEY (`stuid`) REFERENCES `stuinfo` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '入校离校登记表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of inregister
-- ----------------------------
INSERT INTO `inregister` VALUES ('4028b8816b4b97e3016b4bea1d230010', '20151001', '1001', '4028b8816b4b97e3016b4bea1d1d000f', '2019-06-12 08:00:00', '2');
INSERT INTO `inregister` VALUES ('4028b8816b4b97e3016b4bea1d230011', '20151002', '1001', '4028b8816b4b97e3016b4bea1d1d000f', '2019-06-12 08:00:00', '2');
INSERT INTO `inregister` VALUES ('4028b8816b4b97e3016b4bea1d230012', '20151003', '1001', '4028b8816b4b97e3016b4bea1d1d000f', '2019-06-12 08:00:00', '2');
INSERT INTO `inregister` VALUES ('4028b8816b4b97e3016b4bea1d240013', '20151004', '1001', '4028b8816b4b97e3016b4bea1d1d000f', '2019-06-12 08:00:00', '2');

-- ----------------------------
-- Table structure for outregister
-- ----------------------------
DROP TABLE IF EXISTS `outregister`;
CREATE TABLE `outregister`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `stuid` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '学生id',
  `staffid` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '管理员id',
  `rebatchid` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '登记批次id',
  `leavetime` datetime NULL DEFAULT NULL COMMENT '离校时间',
  `city` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '去向',
  `status` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '状态，0 创建 1 历史',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Compact;

-- ----------------------------
-- Records of outregister
-- ----------------------------
INSERT INTO `outregister` VALUES ('4028b8816b4b97e3016b4bede31f0015', '20151001', '1001', '4028b8816b4b97e3016b4bede31a0014', '2019-06-13 08:00:00', '回家', '2');
INSERT INTO `outregister` VALUES ('4028b8816b4b97e3016b4bede31f0016', '20151002', '1001', '4028b8816b4b97e3016b4bede31a0014', '2019-06-13 08:00:00', '回家', '2');
INSERT INTO `outregister` VALUES ('4028b8816b4b97e3016b4bede31f0017', '20151003', '1001', '4028b8816b4b97e3016b4bede31a0014', '2019-06-13 08:00:00', '回家', '2');
INSERT INTO `outregister` VALUES ('4028b8816b4b97e3016b4bede31f0018', '20151004', '1001', '4028b8816b4b97e3016b4bede31a0014', '2019-06-13 08:00:00', '回家', '2');

-- ----------------------------
-- Table structure for registerbatch
-- ----------------------------
DROP TABLE IF EXISTS `registerbatch`;
CREATE TABLE `registerbatch`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '登记批次编号',
  `name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '登记批次名称',
  `staffid` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '管理员id',
  `status` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '入校、离校标识 in out',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '登记批次表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of registerbatch
-- ----------------------------
INSERT INTO `registerbatch` VALUES ('4028b8816b4b97e3016b4bea1d1d000f', '端午放假入校', '1001', 'in');
INSERT INTO `registerbatch` VALUES ('4028b8816b4b97e3016b4bede31a0014', '端午放假离校', '1001', 'out');

-- ----------------------------
-- Table structure for repair
-- ----------------------------
DROP TABLE IF EXISTS `repair`;
CREATE TABLE `repair`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '报修单编号',
  `dormid` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '宿舍号',
  `stuid` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '学生id',
  `reason` int(2) NOT NULL COMMENT '报修类型:0灯1床，桌椅2门窗3其他',
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '报修日期',
  `note` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '说明',
  `status` int(2) NOT NULL COMMENT '0创建1处理中2完成',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `dormid`(`dormid`) USING BTREE,
  INDEX `stuid`(`stuid`) USING BTREE,
  CONSTRAINT `repair_ibfk_1` FOREIGN KEY (`dormid`) REFERENCES `dorm` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `repair_ibfk_2` FOREIGN KEY (`stuid`) REFERENCES `stuinfo` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '报修单' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of repair
-- ----------------------------
INSERT INTO `repair` VALUES ('1', 'A1-103', '20151001', 0, '2019-06-13 00:09:51', 'xxx', 2);

-- ----------------------------
-- Table structure for score
-- ----------------------------
DROP TABLE IF EXISTS `score`;
CREATE TABLE `score`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号',
  `batchid` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '评分批次编号',
  `dormid` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '宿舍号',
  `sanitary` double(4, 2) NULL DEFAULT NULL COMMENT '整齐度',
  `tidy` double(4, 2) NULL DEFAULT NULL COMMENT '卫生',
  `sum` double(4, 2) NULL DEFAULT NULL COMMENT '总分',
  `orderScore` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '排名',
  `status` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '状态，0 创建 1创建完成 2 历史',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `dormid`(`dormid`) USING BTREE,
  INDEX `batchid`(`batchid`) USING BTREE,
  CONSTRAINT `score_ibfk_1` FOREIGN KEY (`dormid`) REFERENCES `dorm` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `score_ibfk_2` FOREIGN KEY (`batchid`) REFERENCES `scoringbatch` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '宿舍评分表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of score
-- ----------------------------
INSERT INTO `score` VALUES ('4028b8816b2cdb99016b2d5ea30d0010', '4028b8816b2cdb99016b2d5ea305000f', 'A1-101', 34.56, 42.37, 76.93, '4', '2');
INSERT INTO `score` VALUES ('4028b8816b2cdb99016b2d5ea30d0011', '4028b8816b2cdb99016b2d5ea305000f', 'A1-102', 35.56, 43.37, 78.93, '3', '2');
INSERT INTO `score` VALUES ('4028b8816b2cdb99016b2d5ea30e0012', '4028b8816b2cdb99016b2d5ea305000f', 'A1-103', 36.56, 44.37, 80.93, '2', '2');
INSERT INTO `score` VALUES ('4028b8816b2cdb99016b2d5ea30e0013', '4028b8816b2cdb99016b2d5ea305000f', 'A1-104', 37.56, 45.37, 82.93, '1', '2');
INSERT INTO `score` VALUES ('4028b8816b2d8537016b2d87c6a70001', '4028b8816b2d8537016b2d87c6800000', 'A1-101', 46.33, 47.34, 93.67, '4', '2');
INSERT INTO `score` VALUES ('4028b8816b2d8537016b2d87c6a90002', '4028b8816b2d8537016b2d87c6800000', 'A1-102', 47.33, 48.34, 95.67, '3', '2');
INSERT INTO `score` VALUES ('4028b8816b2d8537016b2d87c6a90003', '4028b8816b2d8537016b2d87c6800000', 'A1-103', 48.33, 49.34, 97.67, '2', '2');
INSERT INTO `score` VALUES ('4028b8816b2d8537016b2d87c6a90004', '4028b8816b2d8537016b2d87c6800000', 'A1-104', 49.33, 50.34, 99.67, '1', '2');

-- ----------------------------
-- Table structure for scoringbatch
-- ----------------------------
DROP TABLE IF EXISTS `scoringbatch`;
CREATE TABLE `scoringbatch`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '批次编号',
  `name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '评分批次名称',
  `dormname` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '评分批次表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of scoringbatch
-- ----------------------------
INSERT INTO `scoringbatch` VALUES ('4028b8816b2cdb99016b2d5ea305000f', '期中检查', 'A1');
INSERT INTO `scoringbatch` VALUES ('4028b8816b2d8537016b2d87c6800000', '期末检查', 'A1');

-- ----------------------------
-- Table structure for staffinfo
-- ----------------------------
DROP TABLE IF EXISTS `staffinfo`;
CREATE TABLE `staffinfo`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '职工编号',
  `staffname` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '姓名',
  `sex` int(2) NOT NULL COMMENT '0男1女',
  `age` int(3) NOT NULL,
  `dormname` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '宿舍楼名',
  `dormno` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '宿舍楼号',
  `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `staffinfo_ibfk_1`(`dormname`) USING BTREE,
  INDEX `staffinfo_ibfk_2`(`dormno`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '职工信息表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of staffinfo
-- ----------------------------
INSERT INTO `staffinfo` VALUES ('1001', '李冬', 1, 40, 'A', '1', '18779326744');
INSERT INTO `staffinfo` VALUES ('1002', '林芳叶', 1, 42, 'A', '2', '15573679087');
INSERT INTO `staffinfo` VALUES ('1003', '张丹', 1, 45, 'B', '1', '17365711897');
INSERT INTO `staffinfo` VALUES ('1004', '黄晓云', 1, 39, 'B', '2', '15703398489');

-- ----------------------------
-- Table structure for stuinfo
-- ----------------------------
DROP TABLE IF EXISTS `stuinfo`;
CREATE TABLE `stuinfo`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '学号',
  `name` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '姓名',
  `sex` int(2) NOT NULL COMMENT '0男1女',
  `grade` varchar(4) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '年级',
  `academy` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '学院',
  `major` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '专业',
  `dormid` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '宿舍号',
  `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '电话号码',
  `status` int(2) NOT NULL COMMENT '0在校1离校',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `dormid`(`dormid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '学生信息表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of stuinfo
-- ----------------------------
INSERT INTO `stuinfo` VALUES ('20151001', '石佛寺', 0, '2015', '管理学院', '工商管理', 'A1-101', '13487298743', 0);
INSERT INTO `stuinfo` VALUES ('20151002', '张帅', 0, '2015', '管理学院', '工商管理', 'A1-102', '15938210744', 0);
INSERT INTO `stuinfo` VALUES ('20151003', '原廖', 0, '2015', '管理学院', '工商管理', 'A1-103', '15629898098', 0);
INSERT INTO `stuinfo` VALUES ('20151004', '刘长卿', 0, '2015', '管理学院', '工商管理', 'A1-104', '13411768753', 0);
INSERT INTO `stuinfo` VALUES ('20151005', '姜丽', 1, '2015', '管理学院', '工商管理', 'B1-101', '18723298123', 0);
INSERT INTO `stuinfo` VALUES ('20151006', '林峰', 0, '2015', '管理学院', '工商管理', 'A2-104', '15587298010', 0);
INSERT INTO `stuinfo` VALUES ('20151007', '程妲', 1, '2015', '管理学院', '工商管理', 'B1-102', '15388298187', 0);
INSERT INTO `stuinfo` VALUES ('20151008', '江晓', 1, '2015', '管理学院', '工商管理', 'B1-103', '18387287727', 0);
INSERT INTO `stuinfo` VALUES ('20151009', '薏苡仁', 1, '2015', '管理学院', '工商管理', 'B1-104', '15789736188', 0);
INSERT INTO `stuinfo` VALUES ('20151010', '夏晓媛', 1, '2015', '管理学院', '工商管理', 'B1-105', '15593298710', 1);
INSERT INTO `stuinfo` VALUES ('20151011', '王亚伟', 0, '2015', '管理学院', '工商管理', 'A2-101', '15687298755', 0);
INSERT INTO `stuinfo` VALUES ('20151012', '李斯', 0, '2015', '管理学院', '工商管理', 'A2-102', '17629898743', 0);
INSERT INTO `stuinfo` VALUES ('20151013', '于醉', 0, '2015', '管理学院', '工商管理', 'A2-103', '15797729266', 0);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `username` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名',
  `password` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '密码',
  `permission` int(2) NOT NULL COMMENT '0学生1宿管2系统管理员',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `userid`(`username`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户登录信息表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'admin', '123456', 2);
INSERT INTO `user` VALUES ('4028aba26afeb7fb016afed608900000', '1001', '123456', 1);
INSERT INTO `user` VALUES ('4028aba26afeb7fb016afed608910001', '1002', '123456', 1);
INSERT INTO `user` VALUES ('4028aba26afeb7fb016afed608910002', '1003', '123456', 1);
INSERT INTO `user` VALUES ('4028aba26afeb7fb016afed608910003', '1004', '123456', 1);
INSERT INTO `user` VALUES ('4028aba26afeb7fb016afed6762f0004', '20151001', '123456', 0);
INSERT INTO `user` VALUES ('4028aba26afeb7fb016afed6762f0005', '20151002', '123456', 0);
INSERT INTO `user` VALUES ('4028aba26afeb7fb016afed6762f0006', '20151003', '123456', 0);
INSERT INTO `user` VALUES ('4028aba26afeb7fb016afed6762f0007', '20151004', '123456', 0);
INSERT INTO `user` VALUES ('4028aba26afeb7fb016afed6762f0008', '20151005', '123456', 0);
INSERT INTO `user` VALUES ('4028aba26afeb7fb016afed6762f0009', '20151006', '123456', 0);
INSERT INTO `user` VALUES ('4028aba26afeb7fb016afed6762f000a', '20151007', '123456', 0);
INSERT INTO `user` VALUES ('4028aba26afeb7fb016afed6762f000b', '20151008', '123456', 0);
INSERT INTO `user` VALUES ('4028aba26afeb7fb016afed6762f000c', '20151009', '123456', 0);
INSERT INTO `user` VALUES ('4028aba26afeb7fb016afed67630000d', '20151010', '123456', 0);
INSERT INTO `user` VALUES ('4028aba26afeb7fb016afed67630000e', '20151011', '123456', 0);
INSERT INTO `user` VALUES ('4028aba26afeb7fb016afed67630000f', '20151012', '123456', 0);
INSERT INTO `user` VALUES ('4028aba26afeb7fb016afed676300010', '20151013', '123456', 0);

SET FOREIGN_KEY_CHECKS = 1;
